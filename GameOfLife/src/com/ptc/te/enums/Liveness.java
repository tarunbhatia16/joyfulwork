package com.ptc.te.enums;

public enum Liveness {
	DEAD, ALIVE;
}
