package com.ptc.te.enums;

public enum ClockEvent {
	STARTED, TICK, STOPPED;
}
