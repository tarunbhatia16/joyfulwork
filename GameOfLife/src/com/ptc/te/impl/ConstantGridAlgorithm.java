package com.ptc.te.impl;

import com.ptc.te.base.Algorithm;
import com.ptc.te.base.Grid;

public class ConstantGridAlgorithm implements Algorithm {
	// This algorithm does not change the grid, keeps it constant with time
	@Override
	public Grid transitionToNext(Grid previous) {
		return previous;
	}

}
