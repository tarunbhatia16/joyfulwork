package com.ptc.te.impl;

import com.ptc.te.base.GameListener;

public class ConsoleWriter implements GameListener {

	@Override
	public void gameOver() {
	}

	@Override
	public void message(String message) {
		System.out.println(message);
	}

}
