package com.ptc.te.impl;

import com.ptc.te.base.Grid;
import com.ptc.te.enums.Liveness;
import com.ptc.te.exceptions.GridSizeGridConfigMismatchException;
import com.ptc.te.exceptions.InitializationException;
import com.ptc.te.exceptions.ZeroLengthGridException;

public class ArrayGrid extends Grid {
	private Liveness[] cell_array;
	
	public ArrayGrid (int x, int y) throws InitializationException {
		super(x, y);
		cell_array = initializeEmptyArray();
	}
	
	public ArrayGrid (int x, int y, String state) throws InitializationException {
		super(x, y);
		cell_array = convertStringToStateArray (state);
	}
	
	private Liveness[] initializeEmptyArray () throws InitializationException {
		if (x_size * y_size == 0) throw new ZeroLengthGridException();
		
		Liveness[] array = new Liveness[x_size * y_size];
		
		return array;
	}
	
	private Liveness[] convertStringToStateArray (String state) throws InitializationException {
		if (x_size * y_size == 0) throw new ZeroLengthGridException();
		if (x_size * y_size != state.length()) throw new GridSizeGridConfigMismatchException();
		
		Liveness[] array = new Liveness[x_size * y_size];
		for (int index = 0; index < state.length(); index ++)
			array[index] = (state.charAt(index) == '0'? Liveness.DEAD: Liveness.ALIVE);
		
		return array;
	}

	@Override
	public Liveness getState(int x_coor, int y_coor) {
		int index = y_coor * x_size + x_coor;
		return cell_array[index];
	}

	@Override
	public void setState(int x_coor, int y_coor, Liveness state) {
		int index = y_coor * x_size + x_coor;
		cell_array[index] = state;
	}
	
	@Override
	public String toString () {
		StringBuffer buffer = new StringBuffer();
		for (int index=0; index < cell_array.length; index++) {
			if (index % x_size == 0) buffer.append("\n");
			buffer.append(cell_array[index].ordinal());
		}
		return buffer.toString();
	}
}
