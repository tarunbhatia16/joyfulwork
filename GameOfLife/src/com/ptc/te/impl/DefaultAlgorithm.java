package com.ptc.te.impl;

import com.ptc.te.GameRule;
import com.ptc.te.base.Algorithm;
import com.ptc.te.base.Grid;
import com.ptc.te.enums.Liveness;
import com.ptc.te.exceptions.AlgorithmException;
import com.ptc.te.exceptions.InitializationException;
import com.ptc.te.factory.GridFactory;

public class DefaultAlgorithm implements Algorithm {
	// Brute force method
	@Override
	public Grid transitionToNext(Grid previous) throws AlgorithmException {
		int x = previous.getDimX();
		int y = previous.getDimY();
		
		Grid next;
		try {
			next = GridFactory.getGrid(x, y);
		} catch (InitializationException e) {
			throw new AlgorithmException();
		}

		for (int y_coor=0; y_coor < y; y_coor++) {
			for (int x_coor=0; x_coor < x; x_coor++) {
				next.setState(x_coor, y_coor, calculateNextCellState(previous, x_coor, y_coor));
			}
		}
			
		return next;
	}
	
	private Liveness calculateNextCellState (Grid previous, int x, int y) {
		Liveness me = previous.getState(x, y);
		// System.out.println("Calculating next state for " + me + " cell (" + x + "," + y+ ")");
		int x_prev = (x-1>=0?x-1:x);
		int y_prev = (y-1>=0?y-1:y);
		int x_next = (x+1<previous.getDimX()?x+1:x);
		int y_next = (y+1<previous.getDimY()?y+1:y);
		
		int aliveNeighbours = 0;
		for (int y_coor = y_prev; y_coor <= y_next; y_coor++) {
			for (int x_coor = x_prev; x_coor <= x_next; x_coor++) {
				if (!(x_coor==x && y_coor==y) 
						&& previous.getState(x_coor, y_coor) == Liveness.ALIVE) 
					aliveNeighbours ++;
			}
		}
		
		return GameRule.applyRule(me, aliveNeighbours);
	}
}
