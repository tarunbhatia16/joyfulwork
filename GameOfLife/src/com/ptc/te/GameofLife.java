package com.ptc.te;

import java.util.ArrayList;
import java.util.List;

import com.ptc.te.base.Algorithm;
import com.ptc.te.base.Clock;
import com.ptc.te.base.ClockListener;
import com.ptc.te.base.GameListener;
import com.ptc.te.base.GameStateListener;
import com.ptc.te.base.Grid;
import com.ptc.te.constants.Constants;
import com.ptc.te.exceptions.AlgorithmException;
import com.ptc.te.exceptions.InitializationException;
import com.ptc.te.factory.AlgorithmFactory;
import com.ptc.te.factory.GridFactory;
import com.ptc.te.impl.ConsoleWriter;

public class GameofLife implements ClockListener {
	private int x_size;
	private int y_size;
	private int timespan;
	private Grid initial;
	private Algorithm algo;
	private Grid current;
	
	private List<GameListener> gameListeners;
	private List<GameStateListener> stateListeners;
	
	private GameofLife () {
		gameListeners = new ArrayList<GameListener>();
		stateListeners = new ArrayList<GameStateListener>();
	}
	
	public static void play (String dimX, String dimY, String config, String maxage, GameListener listener) {
		GameofLife game = new GameofLife();

		ConsoleWriter console = new ConsoleWriter();
		game.gameListeners.add(console);
		if (listener != null) {
			game.gameListeners.add(listener);
			if (listener instanceof GameStateListener) game.stateListeners.add((GameStateListener)listener);
		}

		try {
			if (maxage != null) {
				game.setIntialState(dimX, dimY, config, maxage);
			} else {
				game.setIntialState(dimX, dimY, config, Constants.DEFAULT_TIMESPAN);		
			}
		} catch (Exception e) {
			e.printStackTrace();
			String message = "Game of Life could not initialized with the specified input! Exiting.";
			message += "An exception occured ...";
			game.notifyGameListeners(message);
			game.over();
			return;
		}
		
		game.algo = AlgorithmFactory.getAlgorithm();
				
		Clock clock = new Clock(game.timespan);
		
		String message = "Clock instantiated with max-age " + game.timespan;
		game.notifyGameListeners(message);

		clock.register(game);

		message = "Clock starting ...";
		game.notifyGameListeners(message);
		clock.startClock();		
	}
	
	public static void main (String[] args) {
		if (args == null || args.length == 0 || args[0] == null || args[1] == null || args[2] == null) {
			System.out.println("Insufficient input! Exiting.");
			return;
		}
		
		if (args.length > 3) {
			play(args[0], args[1], args[2], args[3], null);	
		} else {
			play(args[0], args[1], args[2], Constants.DEFAULT_TIMESPAN, null);
		}
		
	}
	
	private void setIntialState (String x, String y, String state, String maxage) throws InitializationException {
		x_size = Integer.parseInt(x);
		y_size = Integer.parseInt(y);
		initial = GridFactory.getGrid(x_size, y_size, state);
		if (maxage != null) {
			timespan = Integer.parseInt(maxage);
		}
	}

	@Override
	public void tick(int count) {
		try {
			String message = "Clock tick # " + count;
			notifyGameListeners(message);
			current = algo.transitionToNext(current);
			message = "State of grid at time " + count + ": "+ current;
			notifyGameListeners(message);
			if (stateListeners.size() > 0) notifyGameStateListeners(count, current);
		} catch (AlgorithmException e) {
			e.printStackTrace();
			String message = "An exception occured ...\n";
			message += "State of grid at time " + count + " unchanged from time " + (count - 1);
			notifyGameListeners(message);
		}
	}

	@Override
	public void started() {
		current = initial;
		String message = "Initial configuration: " + current;
		notifyGameListeners(message);
		if (stateListeners.size() > 0) notifyGameStateListeners(0, current);
	}

	@Override
	public void stopped() {
		String message = "Clock stopping.";
		notifyGameListeners(message);
		message = "Final configuration: " + current;
		notifyGameListeners(message);
		over();
	}
	
	private void notifyGameListeners (String message) {
		for (GameListener listener: gameListeners) {
			listener.message(message);
		}
	}
	
	private void notifyGameStateListeners (int time, Grid state) {
		for (GameStateListener listener: stateListeners) {
			listener.publishState(time, state);
		}
	}
	
	private void over() {
		for (GameListener listener: gameListeners) {
			listener.gameOver();
		}
	}
}
