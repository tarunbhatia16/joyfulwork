package com.ptc.te.base;

import com.ptc.te.enums.ClockEvent;

public class Clock {
	int time = 0;
	int sleepInterval = 1000;
	int timespan;
	
	public Clock (int timespan) {
		this.timespan = timespan;
	}
	
	ClockListener listener;
	public void register (ClockListener listener) {
		this.listener = listener;
	}	
	public void unregister (ClockListener listener) {
		this.listener = null;
	}
	
	public void startClock() {
		try {
			try {
				notifyListener(ClockEvent.STARTED);
			} catch (Throwable t) {
				t.printStackTrace();
			}
			
			for (time = 1;time <= timespan;time++) {
				try {
					Thread.sleep(sleepInterval);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				try {
					notifyListener(ClockEvent.TICK);
				} catch (Throwable t) {
					t.printStackTrace();
				}
			}
		} finally {
			try {
				notifyListener(ClockEvent.STOPPED);
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}
	}
	
	private void notifyListener (ClockEvent event) {
		if (listener != null) {
			switch (event) {
			case STARTED:
					listener.started();
					break;
			case TICK:
					listener.tick(time);
					break;
			case STOPPED:
					listener.stopped();
					break;
			default:
			}
		}
	}
}
