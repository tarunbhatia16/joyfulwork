package com.ptc.te.base;

public interface ClockListener {
	public void started();
	public void tick (int count);
	public void stopped();
}
