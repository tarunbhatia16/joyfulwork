package com.ptc.te.base;

public interface GameListener {
	public void message (String message);
	public void gameOver ();
}
