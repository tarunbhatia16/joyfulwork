package com.ptc.te.base;

import com.ptc.te.exceptions.AlgorithmException;

public interface Algorithm {
	public Grid transitionToNext (Grid previous) throws AlgorithmException;
}
