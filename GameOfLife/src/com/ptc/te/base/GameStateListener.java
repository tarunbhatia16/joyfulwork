package com.ptc.te.base;

public interface GameStateListener extends GameListener {
	public void publishState (int time, Grid state);
}
