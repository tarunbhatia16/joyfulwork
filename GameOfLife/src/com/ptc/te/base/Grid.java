package com.ptc.te.base;

import com.ptc.te.enums.Liveness;

public abstract class Grid {
	protected int x_size;
	protected int y_size;
	
	public int getDimX () {
		return x_size;
	}
	
	public int getDimY () {
		return y_size;
	}
	
	public Grid (int x_size, int y_size) {
		this.x_size = x_size;
		this.y_size = y_size;
	}

	public abstract Liveness getState(int x_coor, int y_coor);
	public abstract void setState(int x_coor, int y_coor, Liveness state);
}
