package com.ptc.te.factory;

import com.ptc.te.base.Algorithm;
import com.ptc.te.impl.DefaultAlgorithm;

public class AlgorithmFactory {
	public static Algorithm getAlgorithm () {
		return new DefaultAlgorithm();
	}
}
