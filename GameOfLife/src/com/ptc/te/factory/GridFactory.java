package com.ptc.te.factory;

import com.ptc.te.base.Grid;
import com.ptc.te.exceptions.InitializationException;
import com.ptc.te.impl.ArrayGrid;

public class GridFactory {
	public static Grid getGrid (int x, int y) throws InitializationException {
		return new ArrayGrid(x, y);
	}
	
	public static Grid getGrid (int x, int y, String state) throws InitializationException {
		return new ArrayGrid(x, y, state);
	}
}
