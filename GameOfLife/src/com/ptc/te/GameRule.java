package com.ptc.te;

import com.ptc.te.enums.Liveness;

public class GameRule {
	public static Liveness applyRule(Liveness me, int aliveNeighbours) {
		// System.out.println("Rule request for " + me + " cell with " + aliveNeighbours + " alive neighbours ...");
		switch (me) {
		case ALIVE:
			if (aliveNeighbours < 2) return Liveness.DEAD;
			if (aliveNeighbours == 2) return Liveness.ALIVE;
			if (aliveNeighbours == 3) return Liveness.ALIVE;
			return Liveness.DEAD;
		case DEAD:
			if (aliveNeighbours == 3) return Liveness.ALIVE;
			return Liveness.DEAD;
		default:
			return null;
		}
	}
}
