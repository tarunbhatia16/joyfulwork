<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Game of Life: Implementation by Tarun Bhatia</title>
<script src="js/automated.js" ></script>
<link rel="stylesheet" type="text/css" href="css/iframe.css">
</head>
<body>
<h1>Game Monitor</h1>
<table>	
	<tr><td>Dim X:</td><td> <input type="text" name="cols" id="cols" value="<%=request.getParameter("x_size")%>"/></td><td></td></tr>
	<tr><td>Dim Y:</td><td><input type="text" name="rows" id="rows" value="<%=request.getParameter("y_size")%>"/></td><td></td></tr>
	<tr><td>Game Id:</td><td><input type="text" name="gameId" id="gameId" value="<%=request.getParameter("gameId")%>"/></td><td></td></tr>
	<tr><td>Next Time:</td><td><input type="text" name="time" id="time" value="0"/></td><td>Polling interval:</td><td><input type="text" name="poll" id="poll" value="2"/></td></tr>
	<tr><td><input id="startMoniroting" type="button" onclick="startAutomation();" value="Start monitoring!"></input></td><td><input id="stopMonitoring" type="button" onclick="stopAutomation();" value="Stop monitoring!"></input></td></tr>
</table>
<table>	
	<tr>
	<td><iframe id='iFrame' height="400" width="400"></td>
	<td></iframe><iframe id="iFrame_Time" src="iframe_time.html" height="400" width="400"></iframe></td>
	</tr>
</table>
</body>
</html>