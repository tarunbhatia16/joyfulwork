<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Game of Life: Implementation by Tarun Bhatia</title>
<script src="js/grid_automated.js" ></script>
<link rel="stylesheet" type="text/css" href="css/grid.css">
</head>
<body>
<h1>Start a Game</h1>
<form name="tablegen">
<table>	
	<tr><td>Dim X(columns):</td><td> <input type="text" name="cols" id="cols"/></td></tr>
	<tr><td>Dim Y(rows):</td><td><input type="text" name="rows" id="rows"/></td></tr>
	<tr><td colspan="2"><input name="generate" type="button" value="Create Grid!" onclick='createTable();'/></td></tr>
</table>
</form>
<div id="wrapper"></div>
</body>
</html>