var oldtime;

function onIFrameLoad() {
   var serverResponse = document.getElementById("iFrame").contentWindow.document.body.innerHTML;
   var wrapper = document.getElementById('wrapper');
   wrapper.appendChild(serverResponse);
}

function fetchGameState (gameId, time) {
	try {
		var url = new String("iFrameSVG?gameId=");
		url = url.concat(gameId);
		url = url.concat("&time=");
		url = url.concat(time);
		document.getElementById("iFrame").src = url.toString();
	} catch (err) {
	}
}

function fetchNextState () {
	try {
		var gameId = document.getElementById('gameId').value;
		var time = document.getElementById('time').value;
		fetchGameState(gameId, time);
		var value = parseInt(time, 10);
		value = isNaN(value) ? 0 : value;
		value++;
		document.getElementById('time').value = value;
		
		var frame = document.getElementById('iFrame_Time');
		frame.contentDocument.getElementById("iFrame_Time_time").innerHTML = "Time: " + time;
	} catch (err) {
	}
}