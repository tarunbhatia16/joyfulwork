var oldtime;

function onIFrameLoad() {
   var serverResponse = document.getElementById("iFrame").contentWindow.document.body.innerHTML;
   var wrapper = document.getElementById('wrapper');
   wrapper.appendChild(serverResponse);
}

function fetchGameState (gameId, time) {
	try {
		var url = new String("iFrameSVG?gameId=");
		url = url.concat(gameId);
		url = url.concat("&time=");
		url = url.concat(time);
		document.getElementById("iFrame").src = url.toString();
	} catch (err) {
	}
}

function fetchNextState () {
	try {
		var gameId = document.getElementById('gameId').value;
		var time = document.getElementById('time').value;
		fetchGameState(gameId, time);
		var value = parseInt(time, 10);
		value = isNaN(value) ? 0 : value;
		value++;
		document.getElementById('time').value = value;
		
		var frame = document.getElementById('iFrame_Time');
		frame.contentDocument.getElementById("iFrame_Time_time").innerHTML = "Time: " + time;
	} catch (err) {
	}
}

var automator;
function startAutomation () {
	if (automator == undefined) {
		var frame = document.getElementById('iFrame_Time');
		frame.contentDocument.getElementById("iFrame_Time_time").innerHTML = "Time: starting";
		var poll = document.getElementById('poll').value;
		poll = parseInt(poll, 10);
		poll = isNaN(poll) ? 2 : poll;
		poll = poll * 1000;
		automator = setInterval (function() {fetchNextState();}, poll);
	};	
}

function stopAutomation () {
	if (automator) {
		clearInterval (automator);
		automator = undefined;
		var frame = document.getElementById('iFrame_Time');
		frame.contentDocument.getElementById("iFrame_Time_time").innerHTML = "Time: stopped";
	};
}