var oldgrid;
var gridForm;

function createTable() {
	var rows = document.getElementById('rows').value;
	var cols = document.getElementById('cols').value;
	
	var grid = clickableGrid(rows,cols,
			function(el,row,col,i) {
			    if (el.className=='clicked') el.className='';
			    else el.className='clicked';
			});

	var wrapper = document.getElementById('wrapper');
	if (oldgrid) {
		wrapper.replaceChild(grid, oldgrid);
	} else {
		wrapper.appendChild(grid);
		gridForm = createFormForGrid();
		wrapper.appendChild(gridForm);
	}
	
	oldgrid = grid;
}
     
function clickableGrid(rows, cols, callback) {
    var i=0;
    var grid = document.createElement('table');
    grid.className = 'grid';
    for (var r=0;r<rows;++r){
        var tr = grid.appendChild(document.createElement('tr'));
        for (var c=0;c<cols;++c){
            var cell = tr.appendChild(document.createElement('td'));
            cell.innerHTML = ++i;
            cell.addEventListener('click',(function(el,r,c,i){
                return function(){
                    callback(el,r,c,i);
                }
            })(cell,r,c,i),false);
        }
    }
    return grid;
}

function createFormForGrid () {    
    var f = document.createElement("form");
    f.setAttribute('method',"post");
    f.setAttribute('action',"gridStateSubmit");

    var l = document.createElement("label");
    l.setAttribute('for', "maxage");
    l.innerHTML = "Timespan";
    f.appendChild(l);
	addInput(f, 'maxage', 'text');
    
    var s = addInput(f, 'button', 'button');
    s.setAttribute('onclick',"submitGrid();");
    s.setAttribute('value', "Submit Grid");
    
    return f;
}

function calculateGridState () {
	var gridState = new String("");
	if (oldgrid) {
		for (var i = 0, row; row = oldgrid.rows[i]; i++) {
		   for (var j = 0, col; col = row.cells[j]; j++) {
			   if (col.className == 'clicked') 
				   gridState = gridState.concat("1");
			   else
				   gridState = gridState.concat("0");				   
		   };
		};
		
		return gridState;
	};
};

function addInput(theForm, key, type) {
	var input = document.createElement('input');
	input.id = key;
	input.type = type;
	theForm.appendChild(input);
	return input;
}

function submitGrid () {
	if (gridForm) {
		var gridState = calculateGridState();
		
		var params = new String("gridState=");
		params = params.concat(gridState.valueOf());
		params = params.concat("&x_size=");
		params = params.concat(document.getElementById('cols').value);
		params = params.concat("&y_size=");
		params = params.concat(document.getElementById('rows').value);	
		params = params.concat("&maxage=");
		params = params.concat(document.getElementById('maxage').value);
		
		var http = new XMLHttpRequest();
		var url = "gridStateSubmit";
		http.open("POST", url, true);
		
		http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http.setRequestHeader("Content-length", params.length);
		http.setRequestHeader("Connection", "close");
		
		http.onreadystatechange = function() {
			if(http.readyState == 4) {
				var game_response = document.getElementById('game_response');
				game_response.innerHTML = http.responseText;
			}
		};
		
		http.send(params);
	}
}

function createXMLHttpRequest() {
  try { return new ActiveXObject("Msxml2.XMLHTTP");    } catch(e) {}
  try { return new ActiveXObject("Microsoft.XMLHTTP"); } catch(e) {}
  try { return new XMLHttpRequest();                   } catch(e) {}
  alert("XMLHttpRequest not supported");
  return null;
}