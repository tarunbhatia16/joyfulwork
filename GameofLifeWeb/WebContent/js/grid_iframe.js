var oldgrid;
var gridForm;
var rows;
var cols;

function createTable() {
	rows = document.getElementById('rows').value;
	cols = document.getElementById('cols').value;
	
	var grid = clickableGrid(rows,cols,
			function(el,row,col,i) {
			    if (el.className=='clicked') el.className='';
			    else el.className='clicked';
			});

	var wrapper = document.getElementById('wrapper');
	if (oldgrid) {
		wrapper.replaceChild(grid, oldgrid);
	} else {
		wrapper.appendChild(grid);
		gridForm = createFormForGrid();
		wrapper.appendChild(gridForm);
	}
	
	oldgrid = grid;
}
     
function clickableGrid(rows, cols, callback) {
    var i=0;
    var grid = document.createElement('table');
    grid.className = 'grid';
    for (var r=0;r<rows;++r){
        var tr = grid.appendChild(document.createElement('tr'));
        for (var c=0;c<cols;++c){
            var cell = tr.appendChild(document.createElement('td'));
            cell.innerHTML = ++i;
            cell.addEventListener('click',(function(el,r,c,i){
                return function(){
                    callback(el,r,c,i);
                }
            })(cell,r,c,i),false);
        }
    }
    return grid;
}

function createFormForGrid () {    
    var f = document.createElement("form");
    f.setAttribute('method',"post");
    f.setAttribute('action',"gridStateSubmit_iFrame");
    
    var s = addInput(f, 'button', 'button');
    s.setAttribute('onclick',"submitGrid();");
    s.setAttribute('value', "Submit Grid");
    
    var m = addInputBefore(f, 'maxage', 'text', s);
    
    var l = document.createElement('label');
    l.setAttribute('for', "maxage");
    l.innerHTML = "Timespan: ";
    f.insertBefore(l, m);
    
    return f;
}

function calculateGridState () {
	var gridState = new String("");
	if (oldgrid) {
		for (var i = 0, row; row = oldgrid.rows[i]; i++) {
		   for (var j = 0, col; col = row.cells[j]; j++) {
			   if (col.className == 'clicked') 
				   gridState = gridState.concat("1");
			   else
				   gridState = gridState.concat("0");				   
		   };
		};
		
		return gridState;
	};
};

function addInput(theForm, key, type) {
	var input = document.createElement('input');
	input.id = key;
	input.name = key;
	input.type = type;
	theForm.appendChild(input);
	return input;
}

function addInputBefore(theForm, key, type, before) {
	var input = document.createElement('input');
	input.id = key;
	input.name = key;
	input.type = type;
	theForm.insertBefore(input, before);
	return input;
}

function addHidden(theForm, key, value) {
	var input = addInput(theForm, key, 'hidden');
	input.value = value;
}

function submitGrid () {
	if (gridForm) {
		var gridState = calculateGridState();
		addHidden(gridForm, 'gridState', gridState);
		addHidden(gridForm, 'x_size', cols);
		addHidden(gridForm, 'y_size', rows);
		gridForm.submit();
	}
}