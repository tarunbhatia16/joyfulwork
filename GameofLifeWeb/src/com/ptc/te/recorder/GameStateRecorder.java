package com.ptc.te.recorder;

import com.ptc.te.GameofLife;
import com.ptc.te.base.GameStateListener;
import com.ptc.te.base.Grid;

public class GameStateRecorder implements GameStateListener, Runnable {
		String dimX; 
		String dimY;
		String gridState;
		String maxage;
		private Integer gameId;
		private Grid[] states;
		
		public GameStateRecorder (String dimX, String dimY, String gridState, String maxage) {
			this.dimX = dimX;
			this.dimY = dimY;
			this.gridState = gridState;
			this.maxage = maxage;
		}

		@Override
		public void message(String message) {
		}

		@Override
		public void gameOver() {
			// iFrameSolution.mapping.remove(gameId);
		}

		@Override
		public void run() {
			int timespan = Integer.parseInt(maxage);
			setStates(new Grid[timespan+1]);
			GameofLife.play(dimX, dimY, gridState, maxage, this);
		}

		@Override
		public void publishState(int time, Grid state) {
			getStates()[time] = state;
		}

		public Integer getGameId() {
			return gameId;
		}

		public void setGameId(Integer gameId) {
			this.gameId = gameId;
		}

		public Grid[] getStates() {
			return states;
		}

		public void setStates(Grid[] states) {
			this.states = states;
		}	
	}