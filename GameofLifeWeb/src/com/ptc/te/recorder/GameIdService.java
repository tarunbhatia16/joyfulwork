package com.ptc.te.recorder;

import java.util.HashMap;
import java.util.Map;

public class GameIdService {
	private static Map<Integer, GameStateRecorder> mapping = new HashMap<Integer, GameStateRecorder>();; 
	private static int nextGameId;
	
	public static synchronized int getNextGameId() {
		return ++nextGameId;
	}
	
	public static Integer registerGame (GameStateRecorder listener) {
		Integer gameId = new Integer(getNextGameId());
		mapping.put(gameId, listener);
		return gameId;
	}
	
	public static GameStateRecorder getGame (Integer gameId) {
		return mapping.get(gameId); 
	}
}
