package com.ptc.te.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptc.te.GameofLife;
import com.ptc.te.base.GameListener;
import com.ptc.te.base.GameStateListener;
import com.ptc.te.base.Grid;
import com.ptc.te.recorder.GameIdService;
import com.ptc.te.recorder.GameStateRecorder;

/**
 * Servlet implementation class PageStreaming
 */
@WebServlet("/gridStateSubmit_Automated")
public class AutomatedSolution extends HttpServlet {
	@Override
	public void init() throws ServletException {
		super.init();
	}

	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AutomatedSolution() {
        super();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String gridState = request.getParameter("gridState");
		String dimX = request.getParameter("x_size");
		String dimY = request.getParameter("y_size");
		String maxage = request.getParameter("maxage");
		
		GameStateRecorder listener = new GameStateRecorder(dimX, dimY, gridState, maxage);
		Integer gameId = GameIdService.registerGame(listener);
		listener.setGameId(gameId);
		
		Thread thread = new Thread(listener);
		thread.start();
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("game_automated.jsp?gameId=" + gameId);
		dispatcher.forward(request, response);
	}
}