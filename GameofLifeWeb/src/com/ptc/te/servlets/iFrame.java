package com.ptc.te.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptc.te.GameofLife;
import com.ptc.te.base.GameListener;
import com.ptc.te.base.Grid;
import com.ptc.te.recorder.GameIdService;
import com.ptc.te.recorder.GameStateRecorder;

/**
 * Servlet implementation class iFrame
 */
@WebServlet("/iFrame")
public class iFrame extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public iFrame() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("gameId");
		String t = request.getParameter("time");
		
		System.out.println("Received iFrame request for gameId: " + id + ", time: " + t);

		Integer gameId = Integer.parseInt(id);
		Integer time = Integer.parseInt(t);
		
		Grid state = null;
		GameStateRecorder stateListener = GameIdService.getGame(gameId);
		if (stateListener.getStates() != null && stateListener.getStates().length > time) {
			state = stateListener.getStates()[time];
		}
		
		if (state == null) {
			response.setContentType("text/plain");
			PrintWriter out = response.getWriter();
			out.println("Out of Time");
			out.flush();
			return;
		}
		
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		out.println(state);
		out.flush();
	}

}
