package com.ptc.te.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptc.te.GameofLife;
import com.ptc.te.base.GameStateListener;
import com.ptc.te.base.Grid;
import com.ptc.te.svg.Generator;

/**
 * Servlet implementation class PageStreaming
 */
@WebServlet("/gridStateSubmitSVG")
public class PageStreamingSVG extends HttpServlet implements GameStateListener {
	private static final long serialVersionUID = 1L;
	private ThreadLocal<PrintWriter> out = new ThreadLocal<PrintWriter>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PageStreamingSVG() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String gridState = request.getParameter("gridState");
		String dimX = request.getParameter("x_size");
		String dimY = request.getParameter("y_size");
		String maxage = request.getParameter("maxage");
		
		response.setContentType("image/svg+xml");
		
		try {
			out.set(response.getWriter());
		} catch (IOException e) {
			return;
		}
		
		GameofLife.play(dimX, dimY, gridState, maxage, this);
	}

	@Override
	public void gameOver() {
		out.get().close();
		out.set(null);
	}

	@Override
	public void message(String message) {
//		if (out != null) {
//			out.get().println(message);
//			out.get().flush();
//		}
	}

	@Override
	public void publishState(int time, Grid state) {
		Generator svg = new Generator();
		svg.generateSVG(state.getDimX(), state.getDimY(), state);
		svg.stream(out.get());
		out.get().flush();
	}
}
