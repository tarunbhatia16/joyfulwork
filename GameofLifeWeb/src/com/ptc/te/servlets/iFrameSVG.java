package com.ptc.te.servlets;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ptc.te.GameofLife;
import com.ptc.te.base.GameListener;
import com.ptc.te.base.Grid;
import com.ptc.te.recorder.GameIdService;
import com.ptc.te.recorder.GameStateRecorder;
import com.ptc.te.svg.Generator;

/**
 * Servlet implementation class iFrame
 */
@WebServlet("/iFrameSVG")
public class iFrameSVG extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public iFrameSVG() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("gameId");
		String t = request.getParameter("time");
		
		System.out.println("Received iFrame request for gameId: " + id + ", time: " + t);

		Integer gameId = Integer.parseInt(id);
		Integer time = Integer.parseInt(t);
		
		Grid state = null;
		GameStateRecorder stateListener = GameIdService.getGame(gameId);
		if (stateListener == null) {
			print(response, "No such game!");
			return;
		}
		
		if (stateListener.getStates() == null) {
			print(response, "Game has not started!");
			return;
		}
		
		if (stateListener.getStates().length <= time) {			
			print(response, "This is Beyond Time!");
			return;
		}
		
		state = stateListener.getStates()[time];
		
		if (state == null) {
			print(response, "You have arrived at the future! Game clock yet to tick this time.");
			return;
		}
		
		response.setContentType("image/svg+xml");
		Generator svg = new Generator();
		svg.generateSVG(state.getDimX(), state.getDimY(), state);
		PrintWriter out = response.getWriter();
		svg.stream(out);
		out.flush();
	}
	
	private void print (HttpServletResponse response, String message) throws IOException {
		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		out.println(message);
		out.flush();
	}
}
