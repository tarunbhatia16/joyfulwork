package com.ptc.te.svg;

import java.awt.BasicStroke;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Stroke;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.IOException;

import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.svggen.SVGGraphics2DIOException;
import org.apache.batik.dom.GenericDOMImplementation;

import org.w3c.dom.Document;
import org.w3c.dom.DOMImplementation;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

import com.ptc.te.base.Grid;
import com.ptc.te.enums.Liveness;

public class Generator {
	private SVGGraphics2D svg;
	private int maxDim = 400;
	
	public Generator () {
		// Get a DOMImplementation.
		DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

		// Create an instance of org.w3c.dom.Document.
		String svgNS = "http://www.w3.org/2000/svg";
		Document document = domImpl.createDocument(svgNS, "svg", null);

		// Create an instance of the SVG Generator.
		svg = new SVGGraphics2D(document);
	}

	public void generateSVG (int dimX, int dimY, Grid state) {
		paint(svg, dimX, dimY, state);
	}

	public void paint(Graphics2D g2d, int dimX, int dimY, Grid state) {
		int cellDim;
		if (dimX > dimY) {
			cellDim = maxDim / dimX;
		} else {
			cellDim = maxDim / dimY;
		}
		
		g2d.setPaint(Color.black);
		float thickness = 4;
//		Stroke oldStroke = g2d.getStroke();
		g2d.setStroke(new BasicStroke(thickness));
		g2d.drawRect(0, 0, cellDim * dimX, cellDim * dimY);
//		g2d.setStroke(oldStroke);

		for (int x=0; x < dimX; x++) {
			for (int y=0; y < dimY; y++) {
				rectangle(g2d, x, y, state.getState(x, y), cellDim);
			}
		}
	}
	
	private void rectangle (Graphics2D g2d, int x, int y, Liveness cell, int cellDim) {
		Color color = Color.white;
		switch (cell) {
		case ALIVE:
			color = Color.red;
			break;
		case DEAD:
			color = Color.white;
			break;
		}
		
		Rectangle rect = new Rectangle(x * cellDim, y * cellDim, cellDim, cellDim);	
		g2d.setColor(color);
		g2d.fill(rect);
		g2d.setColor(Color.black);
		g2d.draw(rect);
	}

	public void stream (PrintWriter out) {
		// Finally, stream out SVG using UTF-8 encoding.
		boolean useCSS = true; // we want to use CSS style attributes
		
		try {
			svg.stream(out, useCSS);
		} catch (SVGGraphics2DIOException e) {
			e.printStackTrace();
		}
	}
}
